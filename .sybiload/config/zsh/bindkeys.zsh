bindkey "^[[3~" delete-char
bindkey "^[[1;5C" forward-word
bindkey "^[[1;5D" backward-word
bindkey "^[[5~" beginning-of-line
bindkey "^[[6~" end-of-line
