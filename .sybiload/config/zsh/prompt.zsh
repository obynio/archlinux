autoload -U colors && colors

PROMPT="[%{$fg[red]%}%n%{$reset_color%}] %{$fg[blue]%}%1~%{$reset_color%} %# "
